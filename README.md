## Usage
### Train
```
python train.py
```

### Test
```
python play.py
```

## Requirements
* TenforFlow
* numpy
